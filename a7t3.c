#include <stdio.h>
#define MAX 100

int main(){
 int a[MAX][MAX], b[MAX][MAX], product[MAX][MAX];
 int arows, acolumns, brows, bcolumns;
 int i,j,k;
 int sum=0;

 printf("Enter the rows and columns of the matrix a:\n");
 scanf("%d %d",&arows, &acolumns);

 printf("Enter the elements of the matrix a:\n");
 for(i=0; i<arows; i++){
    for(j=0; j<acolumns; j++){
        scanf("%d", &a[i][j]);
    }
 }
 printf("Enter the rows and columns of the matrix b:\n");
 scanf("%d %d", &brows, &bcolumns);

 if(brows != acolumns){
    printf("Error!");
 }
 else{
    printf("Enter the elements of matrix b:\n");
    for (i=0; i<brows; i++){
         for(j=0; j<bcolumns; j++){
            scanf("%d",&b[i][j]);
         }
    }
 }
 printf("\n");
 for(i=0; i<arows; i++){
    for(j=0; j<bcolumns; j++){
        for(k=0; k<brows; k++){
            sum += a[i][k] * b[k][j];
        }
        product[i][j] = sum;
        sum=0;
    }
 }

 printf("Final result matrix:\n");
 for(i=0; i<arows; i++){
    for(j=0; j<bcolumns; j++){
        printf("\t%d",product[i][j]);
    }
    printf("\n");
 }
 return 0;
}


