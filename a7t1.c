#include <stdio.h>
#include <string.h> 
#define SIZE 100

int reverse();

int main(){
 char s[SIZE];
 printf("Enter a sentence: \n");
 fgets (s, SIZE, stdin);
 reverse(s);
}

int reverse(char s[SIZE]){
 int len=strlen(s);
 printf("The reverse sentence:\n");
 for(int a=len;a>=0;a--){
  printf("%c",s[a]);
  }
 }
