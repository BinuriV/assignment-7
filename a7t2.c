#include <stdio.h>
#define SIZE 100

int main(){
 char str[100],ch;
 int count = 0;
 printf("Enter a string:\n");
 fgets(str, SIZE, stdin);

 printf("Enter a character to to the frequency:\n");
 scanf("%c", &ch);

 for(int i=0; str[i] != '\0'; ++i){
    if (ch == str[i])
        ++count;
 }
 printf("Frequency of %c = %d\n",ch, count);
 return 0;
}

